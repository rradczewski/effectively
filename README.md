# 🧐 Effectively - Reveal the inner workings of your app

`effectively` is a collection of libraries that reveal the effective runtime-configuration of an application, enabling you to safely refactor crucial aspects like its deployment process.

## Installation

The latest artifacts can be found on this project's [package registry](https://gitlab.com/rradczewski/effectively/-/packages).

| Name                                    | Language/Framework                                                                                               | Supported config sources                                                 |
| --------------------------------------- | ---------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| [`effectively-core`](./java/core/)      | Any JVM                                                                                                          | Environment variables<br>Java system properties                          |
| [`effectively-play2.8`](./java/play2.8) | [Playframework 2.8](https://www.playframework.com/documentation/2.8.x/Home)                                      | Play Application Config                                                  |
| [`effectively-spring3`](./java/spring3) | [Spring Framework 3.x](https://docs.spring.io/spring/docs/3.2.18.RELEASE/spring-framework-reference/htmlsingle/) | Beans internals<br>Property Values<br>Injected Values                    |
| [`effectively-spring4`](./java/spring4) | [Spring Framework 4.x](https://docs.spring.io/spring/docs/4.3.27.RELEASE/spring-framework-reference/htmlsingle/) | Beans internals<br>Property Values<br>Injected Values                    |
| [`effectively-spring5`](./java/spring5) | [Spring Framework 5.x](https://docs.spring.io/spring/docs/5.2.6.RELEASE/spring-framework-reference/htmlsingle/)  | Beans internals<br>Property Values<br>Injected Values                    |
| [`effectively-js`](./nodejs)            | NodeJS                                                                                                           | Environment variables<br>NodeJS configuration<br>`express` configuration |

### JVM

Add the right dependency for your framewokr. To print out the internals, invoke `dumpWithDefaults` without arguments. Any argument provided needs to be an `EffectivelySource`, e.g. a `Spring5BeansSource(ApplicationContext context)`.

```java
Effectively.dumpWithDefaults();
Effectively.dumpWithDefaults(new Spring5BeansSource(context), new Spring5PropertySource(context));
```

### NodeJS

Package has to be published.

## 💡 Use-case

It is useful whenever you're dealing with legacy applications with complicated deployment processes and you're not quite sure what is actually in effect at runtime (hence the name).

First, we'll gather the actual configuration for each of the environments by integrating effectively. From here, you can change the deployment process, clean-up and refactor. Routinely check if the effective configuration values still match by deploying the app and `diff`-ing the output of `effectively`.

### Creating the golden-master

- Find the right `effectively` implementation for your system  
  (e.g. the [`nodejs`](./nodejs) library or [`play 2.8`](./java/play2.8/))
- Integrate `effectively` so it dumps the configuration right at application startup (TODO: see the guides for each implementation)
- Deploy your application to your environments and retrieve the logs
- Store them for future reference for each environment, e.g. `effectively-$ENV-$DATE-golden-master.conf`

### Refactoring

- Make your changes, e.g. by employing [12-factor-app configuration](https://12factor.net/config), so moving everything to fine-grained environment variables
- Deploy your app again and retrieve the logs
- Use `diff` to find out if all configuration values have been retained properly, e.g. `diff effectively-$ENV-$DATE-golden-master.conf effectively-$ENV-$DATE-after-refactoring.conf`
- Repeat until shiny 💅!

### GOLDEN MASTER: How to make effectively compare a golden master

_This is all wishful-thinking:_

Similar to how snapshots work, `effectively` could read a serialized golden-master at runtime and diff it against the actual values. It can then highlight the differences, and as such skip the manual and sometimes tedious step of retrieving logs from deployed applications.

## 🗄 Libraries

### Nodejs

```js
const myExpressApp = createMyExpressApp();

// ...

const effectively = require("effectively");
effectively.dump({ express: myExpressApp });
```

```
environment.PAGER="less"
environment.PWD="/home/raimo/projects/effectively/nodejs"
environment.SHELL="/bin/zsh"
// ...
express.settings.env="test"
express.settings.etag="weak"
express.settings.etag fn=NOT_SERIALIZABLE_TYPE(function)
express.settings.jsonp callback name="callback"
express.settings.query parser="extended"
express.settings.query parser fn=NOT_SERIALIZABLE_TYPE(function)
express.settings.subdomain offset=2
express.settings.trust proxy=false
express.settings.trust proxy fn=NOT_SERIALIZABLE_TYPE(function)
express.settings.view=NOT_SERIALIZABLE_TYPE(function)
express.settings.views="/home/raimo/projects/effectively/nodejs/views"
express.settings.x-powered-by=true
// ...
global.setInterval=NOT_SERIALIZABLE_TYPE(function)
global.setTimeout=NOT_SERIALIZABLE_TYPE(function)
// ...
process.argv0="node"
process.cwd="/home/raimo/projects/effectively/nodejs"
```
