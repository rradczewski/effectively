const express = require("express");
const effectively = require("./effectively");
const renderer = require("./renderer");

describe("Dumping the configuration of an express app", () => {
  describe("express", () => {
    it("dumps all app settings", () => {
      const app = express();

      app.set("THIS_KEY", "SHOULD_SHOW_UP");

      const config = effectively({ express: app });
      expect(config.express.settings).toHaveProperty(
        "THIS_KEY",
        "SHOULD_SHOW_UP"
      );
    });
  });

  it("dumps all environment variables", () => {
    process.env.THIS_ENV_KEY = "SHOULD_SHOW_UP";

    const config = effectively();
    expect(config.environment).toHaveProperty("THIS_ENV_KEY", "SHOULD_SHOW_UP");
  });

  it("should dump globals", () => {
    global.THIS_GLOBAL_VALUE = "SHOULD_SHOW_UP";

    const config = effectively();
    expect(config.global).toHaveProperty("THIS_GLOBAL_VALUE", "SHOULD_SHOW_UP");
  });

  it("should dump argv", () => {
    const config = effectively();
    expect(config.process.argv).toEqual(process.argv);
  });

  it("should dump the cwd", () => {
    const config = effectively();
    expect(config.process.cwd).toEqual(process.cwd());
  });

  it("should render this test run's environment without an Error being thrown", () => {
    const app = express();

    const config = effectively({ express: app });

    // A lot of stuff injected by jest is some sort of circular tree that just slows down the test
    renderer(config, {
      ignoreKeys: {
        global: {
          _commonForOrigin: true,
          _currentOriginData: true,
          _globalProxy: true,
          _parent: true,
          _sessionHistory: true,
          _top: true,
          frames: true,
          jasmine: true,
          parent: true,
          process: true,
          self: true,
          top: true,
          window: true,
        },
      },
    });
  });
});
