const flatMap = (fn, xs) => xs.reduce((ys, x) => [...ys, ...fn(x)], []);

module.exports = (config, { ignoreKeys = {} } = {}) => {
  const seenObjects = new WeakMap();

  const entriesAsLines = (config, subKeysToIgnore, prefix) =>
    flatMap((key) => {
      if (subKeysToIgnore[key] === true) return [];
      const fullKey = prefix ? `${prefix}.${key}` : key;
      const value = config[key];

      if (typeof value === "function") {
        return [`${fullKey}=NOT_SERIALIZABLE_TYPE(function)`];
      }
      if (typeof value === "object" && value !== null) {
        if (seenObjects.has(value)) {
          return [`${fullKey}=${seenObjects.get(value)}`];
        }
        seenObjects.set(value, fullKey);
        return entriesAsLines(value, subKeysToIgnore[key] || {}, fullKey);
      }

      return [`${fullKey}=${JSON.stringify(value)}`];
    }, Object.keys(config).sort());

  return entriesAsLines(config, ignoreKeys).join("\n");
};
