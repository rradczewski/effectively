const renderer = require("./renderer");

describe("renderer", () => {
  it("should sort alphabetically", () => {
    const str = renderer({ b: "woop", a: "bar" });

    expect(str).toMatch(`a="bar"
b="woop"`);
  });

  it("should ignore functions as they can't be stringified properly", () => {
    const str = renderer({ func: () => {} });

    expect(str).toMatch(`func=NOT_SERIALIZABLE_TYPE(function)`);
  });

  it("should nest the path if the value is an object", () => {
    const str = renderer({ level1: { level2: { level3: "woop" } } });

    expect(str).toMatch(`level1.level2.level3="woop"`);
  });

  it("should repeat array keys and show the index", () => {
    const str = renderer({ a: [1, "d", "a"] });

    expect(str).toMatch(`a.0=1
a.1="d"
a.2="a"`);
  });

  it("allows to specify keys to ignore", () => {
    const str = renderer(
      { a: "hello", b: { nested: "1" } },
      { ignoreKeys: { b: { nested: true } } }
    );

    expect(str).not.toMatch(`b.nested="1"`);
  });
});
