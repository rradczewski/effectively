const sources = {
  express: require("./express"),
};

const gatherGlobalConfig = () => ({
  environment: {
    ...process.env,
  },
  global: {
    ...global,
  },
  process: {
    argv: [...process.argv],
    argv0: process.argv0,
    cwd: process.cwd(),
  },
});

module.exports = (additionalSources = {}) => {
  const globalConfig = gatherGlobalConfig();

  return {
    ...globalConfig,

    ...Object.keys(additionalSources).reduce((result, source) => {
      return {
        ...result,
        [source]: sources[source](additionalSources[source]),
      };
    }, {}),
  };
};
