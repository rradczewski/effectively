package com.gitlab.rradczewski.effectively;

import org.junit.jupiter.api.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasEntry;

public class Spring5XmlBeansSourceTest {

  public static class SimpleXmlConfiguredBean {
    String aProperty;

    public String getaProperty() {
      return aProperty;
    }

    public void setaProperty(String aProperty) {
      this.aProperty = aProperty;
    }
  }

  @Test
  public void it_should_show_values_injected_through_xml() {
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
      "it_should_show_values_injected_through_xml.xml"
    );
    context.refresh();

    assertThat(
      new Spring5BeansSource(context).getConfig(),
      hasEntry(
        context.getBeanNamesForType(SimpleXmlConfiguredBean.class)[0] +
        "#aProperty",
        "theValue"
      )
    );
  }

  public static class AReferencedBean {}

  public static class BeanWithReferencedBean {
    AReferencedBean theField;

    public AReferencedBean getTheField() {
      return theField;
    }

    public void setTheField(AReferencedBean theField) {
      this.theField = theField;
    }
  }

  @Test
  public void it_should_reference_beans_injected_through_xml() {
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
            "it_should_reference_beans_injected_through_xml.xml"
    );
    context.refresh();

    assertThat(
            new Spring5BeansSource(context).getConfig(),
            hasEntry(
                    context.getBeanNamesForType(BeanWithReferencedBean.class)[0] +
                            "#theField",
                    context.getBeanNamesForType(AReferencedBean.class)[0]
            )
    );
  }

}
