package com.gitlab.rradczewski.effectively;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Spring5ContextTest {

  @Test
  public void should_dump_context_on_application_refresh() {
    Spring5Adapter mockAdapter = Mockito.mock(Spring5Adapter.class);
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    context.registerBean(Spring5Listener.class, mockAdapter);
    context.refresh();
    context.start();

    verify(mockAdapter).dumpConfiguration(context);
  }
}
