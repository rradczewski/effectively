package com.gitlab.rradczewski.effectively;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsMapContaining.hasEntry;

import com.sun.org.apache.xml.internal.utils.StringComparable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;

class Spring5BeansSourceTest {

  public static class SimpleBean {}

  @Test
  public void it_should_list_a_custom_bean() {
    GenericApplicationContext context = new GenericApplicationContext();
    context.registerBean("simpleBean", SimpleBean.class);
    context.refresh();

    String simpleBeanClassName = (String) (
      new Spring5BeansSource(context).getConfig().get("simpleBean")
    );
    assertThat(simpleBeanClassName, startsWith(SimpleBean.class.toString()));
  }

}
