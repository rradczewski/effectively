package com.gitlab.rradczewski.effectively;

import java.util.*;
import java.util.stream.Collectors;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;

public class Spring5PropertySource implements EffectivelySource {
  private final ApplicationContext context;

  public Spring5PropertySource(ApplicationContext context) {
    this(
      context,
      new HashSet<>(Arrays.asList("systemEnvironment", "systemProperties"))
    );
  }

  public Spring5PropertySource(
    ApplicationContext context,
    Set<String> sourcesToIgnore
  ) {
    this.context = context;
    this.sourcesToIgnore = sourcesToIgnore;
  }

  @Override
  public String getNamespace() {
    return "spring_properties";
  }

  Set<String> sourcesToIgnore;

  @Override
  public Map<String, Object> getConfig() {
    Environment environment = this.context.getEnvironment();
    if (
      !(environment instanceof ConfigurableEnvironment)
    ) return Collections.emptyMap();

    return ((ConfigurableEnvironment) environment).getPropertySources()
      .stream()
      .filter(source -> !sourcesToIgnore.contains(source.getName()))
      .filter(source -> source instanceof EnumerablePropertySource)
      .map(source -> (EnumerablePropertySource) source)
      .flatMap(
        source ->
          Arrays
            .stream(source.getPropertyNames())
            .collect(
              Collectors.toMap(
                name -> name,
                name ->
                  source.getProperty(name) + " (from: " + source.getName() + ")"
              )
            )
            .entrySet()
            .stream()
      )
      .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }
}
