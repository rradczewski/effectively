package com.gitlab.rradczewski.effectively;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

public class Spring5Listener {
  private Spring5Adapter adapter;

  public Spring5Listener(Spring5Adapter adapter) {
    this.adapter = adapter;
  }

  @EventListener
  public void handleContextRefreshEvent(ContextRefreshedEvent ctxRefreshEvent) {
    this.adapter.dumpConfiguration(ctxRefreshEvent.getApplicationContext());
  }
}
