package com.gitlab.rradczewski.effectively;

import org.springframework.context.ApplicationContext;

public class Spring5Adapter {

  public void dumpConfiguration(ApplicationContext context) {
    try {
      Effectively.dumpWithDefaults(
        new Spring5BeansSource(context),
        new Spring5PropertySource(context)
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
