package com.gitlab.rradczewski.effectively.springbootdemo;

import com.gitlab.rradczewski.effectively.Spring5Adapter;
import com.gitlab.rradczewski.effectively.Spring5Listener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootDemoApplication {

  @Bean
  public Spring5Listener createListener() {
    return new Spring5Listener(new Spring5Adapter());
  }

  public static void main(String[] args) {
    SpringApplication.run(SpringBootDemoApplication.class, args);
  }
}
