package com.gitlab.rradczewski.effectively;

import org.springframework.context.ApplicationContext;

public class Spring4Adapter {

  public void dumpConfiguration(ApplicationContext context) {
    try {
      Effectively.dumpWithDefaults(
              new Spring4BeansSource(context),
              new Spring4PropertySource(context)
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
