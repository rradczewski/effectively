package com.gitlab.rradczewski.effectively;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

public class Spring4Listener {
  private Spring4Adapter adapter;

  public Spring4Listener(Spring4Adapter adapter) {
    this.adapter = adapter;
  }

  @EventListener
  public void handleContextRefreshEvent(ContextRefreshedEvent ctxRefreshEvent) {
    this.adapter.dumpConfiguration(ctxRefreshEvent.getApplicationContext());
  }
}
