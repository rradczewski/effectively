package com.gitlab.rradczewski.effectively;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

public class Spring4BeansSource implements EffectivelySource {
  ApplicationContext context;

  public Spring4BeansSource(ApplicationContext context) {
    this.context = context;
  }

  @Override
  public String getNamespace() {
    return "spring_beans";
  }

  @Override
  public Map<String, Object> getConfig() {
    if (context instanceof ConfigurableApplicationContext) {
      return listBeansByRegistryIterator(
        (ConfigurableApplicationContext) context
      );
    }
    return listBeansByNameOnly(context);
  }

  private Map<String, Object> listBeansByRegistryIterator(
    ConfigurableApplicationContext context
  ) {
    ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();

    Iterable<String> iterable = () -> beanFactory.getBeanNamesIterator();
    Map<String, Object> config = new HashMap<>();

    for (String beanName : iterable) {
      if (!beanFactory.containsBeanDefinition(beanName)) {
        continue;
      }

      BeanDefinition beanDefinition = beanFactory.getMergedBeanDefinition(
        beanName
      );
      if (beanDefinition.isLazyInit() && !beanDefinition.isSingleton()) {
        continue;
      }
      config.put(
        beanName,
        String.format(
          "%s (from: %s)",
          beanFactory.getBean(beanName).getClass().toString(),
          beanDefinition.getResourceDescription()
        )
      );
      addDeclaredProperties(beanFactory, config, beanName);
      addInjectedDependencies(beanFactory, config, beanName);
      addInjectedFields(beanFactory, config, beanName);
    }

    return config;
  }

  private void addDeclaredProperties(
    ConfigurableListableBeanFactory beanFactory,
    Map<String, Object> config,
    String beanName
  ) {
    BeanDefinition bd = beanFactory.getMergedBeanDefinition(beanName);
    for (PropertyValue propertyValue : bd
      .getPropertyValues()
      .getPropertyValues()) {
      String propertyKey = beanName + "#" + propertyValue.getName();
      if (propertyValue.getValue() instanceof RuntimeBeanReference) {
        config.put(
          propertyKey,
          ((RuntimeBeanReference) propertyValue.getValue()).getBeanName()
        );
      } else {
        config.put(propertyKey, propertyValue.getConvertedValue());
      }
    }
  }

  private void addInjectedFields(
    ConfigurableListableBeanFactory beanFactory,
    Map<String, Object> config,
    String beanName
  ) {
    Object bean = beanFactory.getBean(beanName);
    Class<?> type = beanFactory.getType(beanName);

    while (type != null) {
      List<Field> fieldsWithValueAnnotation = Arrays
        .stream(type.getDeclaredFields())
        .filter(
          field ->
            Arrays
              .stream(field.getDeclaredAnnotations())
              .anyMatch(
                annotation -> annotation.annotationType().equals(Value.class)
              )
        )
        .collect(Collectors.toList());

      for (Field field : fieldsWithValueAnnotation) {
        try {
          config.put(beanName + "#" + field.getName(), field.get(bean));
        } catch (IllegalAccessException e) {}
      }
      type = type.getSuperclass();
    }
  }

  private void addInjectedDependencies(
    ConfigurableListableBeanFactory beanFactory,
    Map<String, Object> config,
    String beanName
  ) {
    Object bean = beanFactory.getBean(beanName);
    String[] dependenciesForBean = beanFactory.getDependenciesForBean(beanName);
    Field[] fieldsWithAutowiredAnnotation = allDeclaredFields(bean);

    Arrays
      .stream(dependenciesForBean)
      .forEach(
        depName -> {
          BeanDefinition bd = beanFactory.containsBeanDefinition(depName)
            ? beanFactory.getBeanDefinition(depName)
            : null;

          if (bd == null || (!bd.isSingleton() && bd.isLazyInit())) {
            // Bean is a lazy init bean and not a singleton, so we don't want to initialize it here.
            return;
          }

          Object dependingBean = beanFactory.getBean(depName);
          Class<?> typeOfDependingBean = dependingBean.getClass();

          Optional<Field> fieldOfDependingBean = Arrays
            .stream(fieldsWithAutowiredAnnotation)
            .filter(
              field -> {
                try {
                  return (
                    field.getType().isAssignableFrom(typeOfDependingBean) &&
                    field.get(bean).equals(dependingBean)
                  );
                } catch (IllegalAccessException e) {
                  return false;
                }
              }
            )
            .findFirst();

          if (fieldOfDependingBean.isPresent()) {
            config.put(
              beanName + "#" + fieldOfDependingBean.get().getName(),
              depName
            );
          }
        }
      );
  }

  private Field[] allDeclaredFields(Object bean) {
    Set<Field> fields = new HashSet<>();

    Class<?> currentType = bean.getClass();
    while (currentType != null) {
      Arrays
        .stream(currentType.getDeclaredFields())
        .filter(
          field ->
            Arrays
              .stream(field.getDeclaredAnnotations())
              .anyMatch(
                annotation ->
                  annotation.annotationType().equals(Autowired.class)
              )
        )
        .forEach(
          newField -> {
            boolean containsFieldWithSameName = fields
              .stream()
              .anyMatch(
                existingField ->
                  existingField.getName().equals(newField.getName())
              );
            if (!containsFieldWithSameName) {
              fields.add(newField);
            }
          }
        );
      currentType = currentType.getSuperclass();
    }

    return fields.toArray(new Field[] {  });
  }

  private static Map<String, Object> listBeansByNameOnly(
    ApplicationContext context
  ) {
    return Arrays
      .stream(context.getBeanDefinitionNames())
      .flatMap(resolveAliases(context))
      .collect(mapNamesToClasses(context));
  }

  private static Collector<String, ?, Map<String, Object>> mapNamesToClasses(
    ApplicationContext context
  ) {
    return Collectors.toMap(
      name -> name,
      name -> context.getBean(name).getClass().toString()
    );
  }

  private static Function<String, Stream<? extends String>> resolveAliases(
    ApplicationContext context
  ) {
    return name -> {
      List<String> names = new ArrayList<>(
        Arrays.asList(context.getAliases(name))
      );
      names.add(name);
      return names.stream();
    };
  }
}
