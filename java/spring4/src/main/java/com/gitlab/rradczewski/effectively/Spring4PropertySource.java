package com.gitlab.rradczewski.effectively;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;

public class Spring4PropertySource implements EffectivelySource {
  private final ApplicationContext context;

  public Spring4PropertySource(ApplicationContext context) {
    this(
      context,
      new HashSet<>(Arrays.asList("systemEnvironment", "systemProperties"))
    );
  }

  public Spring4PropertySource(
    ApplicationContext context,
    Set<String> sourcesToIgnore
  ) {
    this.context = context;
    this.sourcesToIgnore = sourcesToIgnore;
  }

  @Override
  public String getNamespace() {
    return "spring_properties";
  }

  Set<String> sourcesToIgnore;

  @Override
  public Map<String, Object> getConfig() {
    Environment environment = this.context.getEnvironment();
    if (
      !(environment instanceof ConfigurableEnvironment)
    ) return Collections.emptyMap();

    return StreamSupport
      .stream(
        ((ConfigurableEnvironment) environment).getPropertySources()
          .spliterator(),
        false
      )
      .filter(source -> !sourcesToIgnore.contains(source.getName()))
      .filter(source -> source instanceof EnumerablePropertySource)
      .map(source -> (EnumerablePropertySource) source)
      .flatMap(
        source ->
          Arrays
            .stream(source.getPropertyNames())
            .collect(
              Collectors.toMap(
                name -> name,
                name ->
                  source.getProperty(name) + " (from: " + source.getName() + ")"
              )
            )
            .entrySet()
            .stream()
      )
      .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }
}
