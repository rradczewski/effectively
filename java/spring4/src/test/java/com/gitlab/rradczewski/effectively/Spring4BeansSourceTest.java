package com.gitlab.rradczewski.effectively;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsMapContaining.hasEntry;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.MapPropertySource;

class Spring4BeansSourceTest {

  public static class SimpleBean {}

  @Test
  public void it_should_list_a_custom_bean() {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    context.register(SimpleBean.class);
    context.refresh();

    String simpleBeanClassName = (String) (
      new Spring4BeansSource(context)
        .getConfig()
        .get(context.getBeanNamesForType(SimpleBean.class)[0])
    );
    assertThat(simpleBeanClassName, startsWith(SimpleBean.class.toString()));
  }
}
