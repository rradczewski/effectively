package com.gitlab.rradczewski.effectively;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasEntry;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.MapPropertySource;

class Spring4AnnotatedBeansSourceTest {

  public static class SimpleBean {}

  public static class BeanWithAutowiredField {
    @Autowired
    SimpleBean theFieldName;
  }

  @Test
  public void it_should_list_the_dependencies_of_a_custom_bean() {
    GenericApplicationContext context = new AnnotationConfigApplicationContext();
    context.registerBeanDefinition(
      "simpleBean",
      BeanDefinitionBuilder
        .genericBeanDefinition(SimpleBean.class)
        .getBeanDefinition()
    );
    context.registerBeanDefinition(
      "beanWithAutowiredField",
      BeanDefinitionBuilder
        .genericBeanDefinition(BeanWithAutowiredField.class)
        .getBeanDefinition()
    );
    context.refresh();

    String qualifierForTheFieldOfTheBeanWithTheAutowiredField =
      context.getBeanNamesForType(BeanWithAutowiredField.class)[0] +
      "#theFieldName";

    String nameOfTheDependingBean = context.getBeanNamesForType(
      SimpleBean.class
    )[0];

    assertThat(
      new Spring4BeansSource(context).getConfig(),
      hasEntry(
        qualifierForTheFieldOfTheBeanWithTheAutowiredField,
        nameOfTheDependingBean
      )
    );
  }

  public static class AnnotatedBeanWithInjectedValue {
    @Value("A plain value")
    String aPlainValue;

    @Value("${a.value.from.properties}")
    String aValueFromProperties;
  }

  @Test
  public void it_should_list_properties_of_a_custom_bean() {
    GenericApplicationContext context = new AnnotationConfigApplicationContext();

    Map<String, Object> customProps = new HashMap<>();
    customProps.put("a.value.from.properties", "thisIsAKValueFromProperties");
    context
      .getEnvironment()
      .getPropertySources()
      .addLast(new MapPropertySource("custom", customProps));

    context.registerBeanDefinition(
      "annotatedBeanWithInjectedValue",
      BeanDefinitionBuilder
        .genericBeanDefinition(AnnotatedBeanWithInjectedValue.class)
        .getBeanDefinition()
    );
    context.refresh();

    Map<String, Object> config = new Spring4BeansSource(context).getConfig();
    assertThat(
      config,
      hasEntry(
        context.getBeanNamesForType(AnnotatedBeanWithInjectedValue.class)[0] +
        "#aPlainValue",
        "A plain value"
      )
    );
    assertThat(
      config,
      hasEntry(
        context.getBeanNamesForType(AnnotatedBeanWithInjectedValue.class)[0] +
        "#aValueFromProperties",
        "thisIsAKValueFromProperties"
      )
    );
  }
}
