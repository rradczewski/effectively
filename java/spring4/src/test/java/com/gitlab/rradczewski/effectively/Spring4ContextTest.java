package com.gitlab.rradczewski.effectively;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Spring4ContextTest {

  @Test
  public void should_dump_context_on_application_refresh() {
    Spring4Adapter mockAdapter = Mockito.mock(Spring4Adapter.class);
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    context.registerBeanDefinition(
      "spring4Listener",
      BeanDefinitionBuilder
        .genericBeanDefinition(Spring4Listener.class)
        .addConstructorArgValue(mockAdapter)
        .getBeanDefinition()
    );
    context.refresh();
    context.start();

    verify(mockAdapter).dumpConfiguration(context);
  }
}
