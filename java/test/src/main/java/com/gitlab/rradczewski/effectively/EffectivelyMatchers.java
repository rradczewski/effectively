package com.gitlab.rradczewski.effectively;

import static org.hamcrest.Matchers.*;

import java.util.Map;
import org.hamcrest.Matcher;

public class EffectivelyMatchers {

  public static Matcher<Map<? extends String, ? extends Map<? extends String, ?>>> hasConfigEntryAtNamespace(
    String namespace,
    String key,
    Object value
  ) {
    return allOf(
      hasKey(namespace),
      hasEntry(
        is(equalTo(namespace)),
        hasEntry(is(equalTo(key)), is(equalTo(value)))
      )
    );
  }
}
