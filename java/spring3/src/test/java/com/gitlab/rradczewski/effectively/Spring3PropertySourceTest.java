package com.gitlab.rradczewski.effectively;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasEntry;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.MapPropertySource;

class Spring3PropertySourceTest {

  @Test
  public void it_should_list_properties_of_a_custom_bean() {
    GenericApplicationContext context = new AnnotationConfigApplicationContext();
    Map<String, Object> customProps = new HashMap<>();
    customProps.put("a.value.from.properties", "thisIsAKValueFromProperties");
    context
      .getEnvironment()
      .getPropertySources()
      .addLast(new MapPropertySource("custom", customProps));

    context.refresh();

    assertThat(
      new Spring3PropertySource(context).getConfig(),
      hasEntry(
        "a.value.from.properties",
        "thisIsAKValueFromProperties (from: custom)"
      )
    );
  }
}
