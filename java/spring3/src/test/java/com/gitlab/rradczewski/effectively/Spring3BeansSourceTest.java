package com.gitlab.rradczewski.effectively;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;

class Spring3BeansSourceTest {

  public static class SimpleBean {}

  @Test
  public void it_should_list_a_custom_bean() {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    context.register(SimpleBean.class);
    context.refresh();

    String simpleBeanClassName = (String) (
      new Spring3BeansSource(context)
        .getConfig()
        .get(context.getBeanNamesForType(SimpleBean.class)[0])
    );
    assertThat(simpleBeanClassName, startsWith(SimpleBean.class.toString()));
  }
}
