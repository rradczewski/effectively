package com.gitlab.rradczewski.effectively;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public class Spring3Listener
  implements ApplicationListener<ContextRefreshedEvent> {
  private Spring3Adapter adapter;

  public Spring3Listener(Spring3Adapter adapter) {
    this.adapter = adapter;
  }

  @Override
  public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
    this.adapter.dumpConfiguration(
        contextRefreshedEvent.getApplicationContext()
      );
  }
}
