package com.gitlab.rradczewski.effectively;

import org.springframework.context.ApplicationContext;

public class Spring3Adapter {

  public void dumpConfiguration(ApplicationContext context) {
    try {
      Effectively.dumpWithDefaults(
        new Spring3BeansSource(context),
        new Spring3PropertySource(context)
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
