package com.gitlab.rradczewski.effectively;

import java.util.HashMap;
import java.util.Map;

class EnvironmentSource implements EffectivelySource {

  @Override
  public String getNamespace() {
    return "environment";
  }

  @Override
  public Map<String, Object> getConfig() {
    HashMap<String, Object> config = new HashMap<>();
    config.putAll(System.getenv());
    return config;
  }
}
