package com.gitlab.rradczewski.effectively;

import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Effectively {
  static EffectivelySource[] defaultSources = new EffectivelySource[] {
    new EnvironmentSource(),
    new SystemPropertiesSource(),
  };

  public static Map<String, Map<String, Object>> gatherWithDefaults(
    EffectivelySource... additionalSources
  ) {
    EffectivelySource[] mergedSources = Stream
      .concat(Arrays.stream(defaultSources), Arrays.stream(additionalSources))
      .toArray(EffectivelySource[]::new);
    return new Effectively(mergedSources).gatherConfig();
  }

  public static void dumpWithDefaults(EffectivelySource... additionalSources) {
    EffectivelySource[] mergedSources = Stream
      .concat(Arrays.stream(defaultSources), Arrays.stream(additionalSources))
      .toArray(EffectivelySource[]::new);
    new Effectively(mergedSources).dumpConfig();
  }

  EffectivelySource[] sources;

  public Effectively(EffectivelySource[] sources) {
    this.sources = sources;
  }

  public void dumpConfig() {
    dumpConfig(System.out);
  }

  public void dumpConfig(PrintStream out) {
    out.println(
      gatherWithDefaults(this.sources)
        .entrySet()
        .stream()
        .flatMap(
          namespaceEntry ->
            namespaceEntry
              .getValue()
              .entrySet()
              .stream()
              .map(
                stringObjectEntry ->
                  String.format(
                    "%s/%s=%s",
                    namespaceEntry.getKey(),
                    stringObjectEntry.getKey(),
                    stringObjectEntry.getValue()
                  )
              )
        )
        .sorted()
        .collect(Collectors.joining("\n"))
    );
  }

  public Map<String, Map<String, Object>> gatherConfig() {
    List<EffectivelySource> allSources = new ArrayList<>();
    allSources.addAll(Arrays.asList(sources));

    Map<String, Map<String, Object>> config = new HashMap();

    for (EffectivelySource source : allSources) {
      config.put(source.getNamespace(), source.getConfig());
    }

    return config;
  }
}
