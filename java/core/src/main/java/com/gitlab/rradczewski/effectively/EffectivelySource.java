package com.gitlab.rradczewski.effectively;

import java.util.Map;

public interface EffectivelySource {
  String getNamespace();
  Map<String, Object> getConfig();
}
