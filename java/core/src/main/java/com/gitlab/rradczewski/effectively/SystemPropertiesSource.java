package com.gitlab.rradczewski.effectively;

import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

class SystemPropertiesSource implements EffectivelySource {

  @Override
  public String getNamespace() {
    return "system_properties";
  }

  @Override
  public Map<String, Object> getConfig() {
    Properties properties = System.getProperties();
    return properties
      .stringPropertyNames()
      .stream()
      .collect(Collectors.toMap(x -> x, properties::getProperty));
  }
}
