package com.gitlab.rradczewski.effectively;

public interface RenderableValue {
  public String renderToString();

  public Object value();
}
