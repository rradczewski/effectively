package com.gitlab.rradczewski.effectively;

import static com.gitlab.rradczewski.effectively.EffectivelyMatchers.hasConfigEntryAtNamespace;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.lang.reflect.Field;
import java.util.Map;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EffectivelyTest {
  private Map<String, Map<String, Object>> config;

  @SuppressWarnings({ "unchecked" })
  public static void updateEnv(String name, String val)
    throws ReflectiveOperationException {
    Map<String, String> env = System.getenv();
    Field field = env.getClass().getDeclaredField("m");
    field.setAccessible(true);
    ((Map<String, String>) field.get(env)).put(name, val);
  }

  @BeforeEach
  public void setupEnv() throws ReflectiveOperationException {
    updateEnv("AN_ENV_KEY", "AN_ENV_VALUE");
  }

  @BeforeAll
  public static void setupSysProp() {
    System.setProperty("A_SYSPROP", "ITS_VALUE");
  }

  @BeforeEach
  public void setupConfig() {
    config = Effectively.gatherWithDefaults();
  }

  @Test
  public void should_dump_environment_values() {
    String namespace = "environment";
    String key = "AN_ENV_KEY";
    assertThat(
      config,
      hasConfigEntryAtNamespace(namespace, key, "AN_ENV_VALUE")
    );
  }

  @Test
  public void should_dump_java_system_properties() {
    Object value =
      ((Map<String, Object>) config.get("system_properties")).get("A_SYSPROP");
    assertThat(value, is(equalTo("ITS_VALUE")));
  }
}
