package com.gitlab.rradczewski.effectively;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.WithApplication;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PlayTest extends WithApplication {
  private Map<String, Map<String, Object>> config;

  @Override
  protected Application provideApplication() {
    Map<String, Object> conf = new HashMap<>();
    conf.put("A_DIRECT_CONFIG_KEY", "THE_VALUE");
    conf.put("AN_ARRAY", Arrays.asList("VALUE_ONE", "VALUE_TWO"));
    // AN_INTERPOLATED_ENV_KEY is in conf/application.conf because mocking Play's config is a nightmare
    Config config = ConfigFactory.parseMap(conf);

    return new GuiceApplicationBuilder().configure(config).build();
  }

  @SuppressWarnings({ "unchecked" })
  public static void updateEnv(String name, String val)
    throws ReflectiveOperationException {
    Map<String, String> env = System.getenv();
    Field field = env.getClass().getDeclaredField("m");
    field.setAccessible(true);
    ((Map<String, String>) field.get(env)).put(name, val);
  }

  // Needs to be static and @BeforeClass because we need to set the ENV value _before_ WithApplication.startPlay is run
  @BeforeClass
  public static void setupEnv() throws ReflectiveOperationException {
    updateEnv("AN_ENV_KEY", "AN_ENV_VALUE");
  }

  @Before
  public void setupConfig() {
    config = Effectively.gatherWithDefaults(new EffectivelyPlaySource(app));
  }

  @Test
  public void should_dump_a_primitive_in_the_play_configuration() {
    EffectivelyPlaySource.PlayConfigValue value = (EffectivelyPlaySource.PlayConfigValue) (
      (Map<String, Object>) config.get("play")
    ).get("A_DIRECT_CONFIG_KEY");
    assertThat(value.renderToString(), is(equalTo("\"THE_VALUE\"")));
  }

  @Test
  public void should_dump_an_array_in_the_play_configuration() {
    EffectivelyPlaySource.PlayConfigValue value = (EffectivelyPlaySource.PlayConfigValue) (
      (Map<String, Object>) config.get("play")
    ).get("AN_ARRAY");
    assertThat(
      ((ConfigValue) value.value()).unwrapped(),
      is(equalTo(Arrays.asList("VALUE_ONE", "VALUE_TWO")))
    );
  }

  @Test
  public void should_interpolate_placeholders_in_play_configuration() {
    EffectivelyPlaySource.PlayConfigValue value = (EffectivelyPlaySource.PlayConfigValue) (
      (Map<String, Object>) config.get("play")
    ).get("AN_INTERPOLATED_ENV_KEY");
    assertThat(
      ((ConfigValue) value.value()).unwrapped(),
      is(equalTo("AN_ENV_VALUE"))
    );
  }
}
