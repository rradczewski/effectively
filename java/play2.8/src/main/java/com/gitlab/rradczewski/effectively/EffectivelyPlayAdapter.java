package com.gitlab.rradczewski.effectively;

import javax.inject.Inject;
import javax.inject.Singleton;
import play.Application;

@Singleton
public class EffectivelyPlayAdapter {

  @Inject
  public EffectivelyPlayAdapter(Application application) {
    Effectively.dumpWithDefaults(new EffectivelyPlaySource(application));
  }
}
