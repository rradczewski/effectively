package com.gitlab.rradczewski.effectively;

import com.typesafe.config.ConfigRenderOptions;
import com.typesafe.config.ConfigValue;
import play.Application;

import java.util.HashMap;
import java.util.Map;

public class EffectivelyPlaySource implements EffectivelySource {
  private final Application app;

  public EffectivelyPlaySource(Application app) {
    this.app = app;
  }

  @Override
  public String getNamespace() {
    return "play";
  }

  @Override
  public Map<String, Object> getConfig() {
    Map<String, Object> config = new HashMap<>();
    for (Map.Entry<String, ConfigValue> entry : app.config().entrySet()) {
      config.put(entry.getKey(), new PlayConfigValue(entry.getValue()));
    }
    return config;
  }

  static class PlayConfigValue implements RenderableValue {
    ConfigValue value;

    PlayConfigValue(ConfigValue value) {
      this.value = value;
    }

    @Override
    public String renderToString() {
      return value.render();
    }

    @Override
    public Object value() {
      return value;
    }

    @Override
    public String toString() {
      return value.render(ConfigRenderOptions.concise());
    }
  }
}
