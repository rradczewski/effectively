package com.gitlab.rradczewski.effectively;

import play.api.Configuration;
import play.api.Environment;
import play.api.inject.Binding;
import play.api.inject.Module;
import scala.collection.Seq;

public class EffectivelyPlayModule extends Module {

  @Override
  public Seq<Binding<?>> bindings(
    Environment environment,
    Configuration configuration
  ) {
    return seq(bind(EffectivelyPlayAdapter.class).toSelf().eagerly());
  }
}
