name := """play-java-seed"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"
resolvers += Resolver.mavenLocal

libraryDependencies += guice
libraryDependencies += "com.gitlab.rradczewski.effectively" % "effectively-play2.8" % "0.0.1-SNAPSHOT"
libraryDependencies += "com.gitlab.rradczewski.effectively" % "effectively-test" % "0.0.1-SNAPSHOT" % "test"
libraryDependencies += "org.hamcrest" % "hamcrest" % "2.2" % "test"